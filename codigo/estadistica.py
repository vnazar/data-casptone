import os, tarfile
import pandas as pd


if __name__ == "__main__":

    separator = "\\"
    path = 'E:\\UAI\\capstone\\data-casptone\\data'
    #path = 'C:\\opt\\UAI\\data-casptone\\data'

    dic_csv = dict()

    my_list = os.listdir(path)

    #### obtenemos la data
    for folder_name in my_list:
        f = os.path.join(path, folder_name)
        if os.path.isdir(f):
            dir_list = os.listdir(f)

            for file in dir_list:
                finalFile = path + separator + folder_name + separator + file
                if ".csv" in file:
                    df = pd.read_csv(finalFile, index_col=0)
                    dic_csv[folder_name] = df




    print()