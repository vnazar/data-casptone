import os, tarfile
from gzip import GzipFile
import pandas as pd
import time

def transform_datetime(fix_datetime):
    date_str, time_str = fix_datetime.split("-")
    return date_str[:4] + "-" + date_str[4:6] + "-" + date_str[6:8] + " " + time_str.replace(".", ",")

def parse_fix_file(filename_read, filename_write, df, folder_name):

    symbol_book = dict()
    fpr = GzipFile(filename_read, "rb")
    start_time = time.time()

    while True:
        try:

            pieces = next(fpr).decode("utf-8").split("\x01")
            md_entry_type = ""

            symbol_dict_aux = dict(Symbol="", ExDestination="", Date="", open = "", close="", high="", low="", imbalance="", amount="", volume="", vwap="", CS="", mDReqID= "")

            for piece in pieces:

                if piece.startswith("35="):
                    symbol_dict_aux["msg_type"] = piece.split("35=")[1]
                elif piece.startswith("207="):
                    symbol_dict_aux["ExDestination"] = piece.split("207=")[1]
                elif piece.startswith("52="):
                    symbol_dict_aux["Date"] = transform_datetime(piece.split("52=")[1])
                elif piece.startswith("55="):
                    symbol_dict_aux["Symbol"] = piece.split("55=")[1]
                elif piece.startswith("269="):
                    if piece.split("269=")[1] == "2":
                        md_entry_type = "trade"
                    elif piece.split("269=")[1] == "4":
                        md_entry_type = "open"
                    elif piece.split("269=")[1] == "5":
                        md_entry_type = "close"
                    elif piece.split("269=")[1] == "7":
                        md_entry_type = "high"
                    elif piece.split("269=")[1] == "8":
                        md_entry_type = "low"
                    elif piece.split("269=")[1] == "9":
                        md_entry_type = "vwap"
                    elif piece.split("269=")[1] == "A":
                        md_entry_type = "imbalance"
                    elif piece.split("269=")[1] == "B":
                        md_entry_type = "volume"
                    elif piece.split("269=")[1] == "D":
                        md_entry_type = "amount"
                    elif piece.split("269=")[1] == "G":
                        md_entry_type = "threshold"
                    elif piece.split("269=")[1] == "S":
                        md_entry_type = "none"

                elif piece.startswith("270="):
                    symbol_dict_aux[md_entry_type] = piece.split("270=")[1]

                elif piece.startswith("271="):
                    if md_entry_type == "volume":
                        symbol_dict_aux["volume"] = piece.split("271=")[1]

                elif piece.startswith("811="):
                    if md_entry_type == "imbalance":
                        symbol_dict_aux["imbalance"] = piece.split("811=")[1]

                elif piece.startswith("262="):
                    symbol_dict_aux["mDReqID"] = piece.split("262=")[1]
                elif piece.startswith("167"):
                    symbol_dict_aux["CS"] = piece.split("167=")[1]

            if symbol_dict_aux["msg_type"] == "X" or symbol_dict_aux["msg_type"] == "W":
                df = create_book(symbol_dict_aux,df, symbol_book)



        except (KeyboardInterrupt, StopIteration):
            break

        except Exception as exc:
            print("Error al leer archivo:", exc)
            break

    df.to_csv(filename_write + folder_name + ".csv")
    print("--- %s seconds --- " % (time.time() - start_time), filename_write)

def create_book(symbol_dict_aux, df, symbol_book):

    if symbol_dict_aux["mDReqID"] in symbol_book:
        estadistic_symbol = symbol_book[symbol_dict_aux["mDReqID"]]
        update = update_data(estadistic_symbol, symbol_dict_aux)

        if(update):

            df2 = {'Symbol' : estadistic_symbol["Symbol"], 'ExDestination' : estadistic_symbol["ExDestination"], 'Date' : estadistic_symbol["Date"],
                   'open' : estadistic_symbol["open"], 'close' : estadistic_symbol["close"], 'high' : estadistic_symbol["high"],
                   'low' : estadistic_symbol["low"], 'imbalance' : estadistic_symbol["imbalance"], 'amount' : estadistic_symbol["amount"],
                   'volume' : estadistic_symbol["volume"], 'vwap' : estadistic_symbol["vwap"],
                   'CS' : estadistic_symbol["CS"], 'mDReqID' : estadistic_symbol["mDReqID"]}

            df = df.append(df2, ignore_index=True)
    else:
        symbol_book[symbol_dict_aux["mDReqID"]] = symbol_dict_aux

    return df

def update_data(book, symbol_dict_aux):

    update = False

    book["Date"] = symbol_dict_aux["Date"]

    if(symbol_dict_aux["Symbol"] != "" and  book["Symbol"] =="" ):
        book["Symbol"] = symbol_dict_aux["Symbol"]

    if (symbol_dict_aux["ExDestination"] != "" and book["ExDestination"] == ""):
        book["ExDestination"] = symbol_dict_aux["ExDestination"]

    if (symbol_dict_aux["CS"] != "" and book["CS"] == ""):
        book["CS"] = symbol_dict_aux["CS"]

    if (symbol_dict_aux["mDReqID"] != "" and book["mDReqID"] == ""):
        book["mDReqID"] = symbol_dict_aux["mDReqID"]

    if (symbol_dict_aux["open"] != ""):

        if book["open"] != symbol_dict_aux["open"]:
            book["open"] = symbol_dict_aux["open"]
            update = True

    if (symbol_dict_aux["low"] != ""):
        if book["low"] != symbol_dict_aux["low"]:
            book["low"] = symbol_dict_aux["low"]
            update = True

    if (symbol_dict_aux["close"] != ""):
        if book["close"] != symbol_dict_aux["close"]:
            book["close"] = symbol_dict_aux["close"]
            update = True

    if (symbol_dict_aux["high"] != ""):
        if book["high"] != symbol_dict_aux["high"]:
            book["high"] = symbol_dict_aux["high"]
            update = True

    if (symbol_dict_aux["imbalance"] != ""):
        if book["imbalance"] != symbol_dict_aux["imbalance"]:
            book["imbalance"] = symbol_dict_aux["imbalance"]
            update = True

    if (symbol_dict_aux["amount"] != ""):

        if book["amount"] != symbol_dict_aux["amount"]:
            book["amount"] = symbol_dict_aux["amount"]
            update = True

    if (symbol_dict_aux["volume"] != ""):

        if book["volume"] != symbol_dict_aux["volume"]:
            book["volume"] = symbol_dict_aux["volume"]
            update = True

    if (symbol_dict_aux["vwap"] != ""):

        if book["vwap"] != symbol_dict_aux["vwap"]:
            book["vwap"] = symbol_dict_aux["vwap"]
            update = True

    return update

if __name__ == "__main__":

    separator = "\\"
    path = 'E:\\UAI\\capstone\\data-casptone\\data'
    #path = 'C:\\opt\\UAI\\data-casptone\\data'

    my_list = os.listdir(path)

    for folder_name in my_list:
        f = os.path.join(path, folder_name)
        if os.path.isdir(f):
            dir_list = os.listdir(f)

            for file in dir_list:

                size = os.path.getsize(path + separator + folder_name + separator + file);

                if (size >= 2000000 and "sdc-bcs-rv-" in file):

                    fileFinal = path + separator + folder_name + separator + file
                    fileoutput = path + separator + folder_name + separator

                    #### extrae los archivos necesarios de la data original (hice cambios de carpeta validar codigo)
                    #try:
                    #    pathoutput = 'E:\\UAI\\capstone\\data-final'
                    #    fileFinalbkp = pathoutput + separator + folder_name + separator + file
                    #    os.mkdir(pathoutput + separator + folder_name)
                    #except OSError as error:
                    #    print(error)
                    #shutil.copyfile(fileFinal, fileFinalbkp)

                    filterfolder = 20211222

                    if int(folder_name) == filterfolder:
                        df = pd.DataFrame(columns=['Symbol','ExDestination','Date','open','close','high', 'low','imbalance','amount','volume','vwap','CS','mDReqID'])
                        parse_fix_file(fileFinal, fileoutput, df, folder_name)
                    else :
                        print("no contempla esa carpeta", folder_name)